<?php

class contratista_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 
     * @param type $table
     * @param type $field
     * @param type $prm
     * @param type $row
     * @return type
     */
    private function _exec_get_where($table, $field, $prm = NULL, $row = false) {
        $result = NULL;

        if (!empty($field)) {
            $this->db->select($field);
        }

        if (is_array($prm)) {
            $resultset = $this->db->get_where($table, $prm);
        } else {
            $resultset = $this->db->get_where($table);
        }

        if ($resultset) {
            if ($row) {
                $result = $resultset->row_array();
            } else {
                $result = $resultset->result_array();
            }
        }

        return $result;
    }

    /**
     * 
     * @param type $sql
     * @param type $prm
     * @return type
     */
    private function _exec_query($sql, $prm = NULL, $row = false) {
        $result = NULL;

        if (is_array($prm)) {
            $resultset = $this->db->query($sql, $prm);
        } else {
            $resultset = $this->db->query($sql);
        }

        if ($resultset) {
            if ($row) {
                $result = $resultset->row_array();
            } else {
                $result = $resultset->result_array();
            }
        }
        return $result;
    }

    /**
     * 
     * @param type $co_contratista
     * @return type
     */
    protected function existContratistaCodigo($co_contratista) {
        $fields = "co_contratista";
        $resultset = $this->_exec_get_where("a001v_contratista", $fields, array("co_contratista" => $co_contratista),true);
        return !empty($resultset);
    }

    /**
     * 
     * @param type $co_tipo_rif
     * @param type $nu_rif
     * @return type
     */
    protected function existContratistaRif($co_tipo_rif, $nu_rif) {
        $fields = "co_tipo_rif, nu_rif";
        $resultset = $this->_exec_get_where("a001v_contratista", $fields, array("co_tipo_rif" => $co_tipo_rif, "nu_rif" => $nu_rif),true);
        return !empty($resultset);
    }

    /**
     * 
     * @param type $co_contratista
     * @param type $tx_cedula_pasaporte
     * @return type
     */
    protected function existRepresentanteContratista($co_contratista, $tx_cedula_pasaporte) {
        $fields = "co_representante_legal, tx_cedula_pasaporte";
        $resultset = $this->_exec_get_where("i021t_representante_legal", $fields, array("tx_cedula_pasaporte" => $tx_cedula_pasaporte, "co_contratista" => $co_contratista), true);
        return !empty($resultset);
    }

    /**
     * 
     * @param type $co_contratista
     * @return type
     */
    public function selectContratista($co_contratista) {

        return $this->_exec_get_where("a001v_contratista", NULL, array("co_contratista" => $co_contratista));
    }

    /**
     * 
     * @param type $co_tipo_rif
     * @param type $nu_rif
     * @param type $pw_contrasena
     * @return type
     */
    public function selectUsuarioAutoriza($co_tipo_rif, $nu_rif, $pw_contrasena) {
        $fields = "co_contratista";
        return $this->_exec_get_where("a001v_contratista", $fields, array("co_tipo_rif" => $co_tipo_rif, "nu_rif" => $nu_rif, "pw_contrasena" => $pw_contrasena));
    }

    /**
     * 
     * @return type
     */
    public function selectLocalidadesMaestras() {
        $sql = "SELECT localidad.co_localidad, localidad.tx_localidad "
                . " FROM i001t_localidad localidad "
                . " INNER JOIN i002t_tipo_localidad tipo_localidad ON localidad.co_tipo_localidad = tipo_localidad.co_tipo_localidad AND tipo_localidad.co_tipo_localidad_padre = 0";
        return $this->_exec_query($sql);
    }

    /**
     * 
     * @param type $co_localidad
     * @return type
     */
    public function selectLocalidad($co_localidad_padre, $tx_tipo_localidad) {

        $sql = " SELECT loc_hijo.co_localidad, loc_hijo.co_tipo_localidad, loc_hijo.tx_localidad "
                . " FROM sigesrac.i001t_localidad loc_padre "
                . " INNER JOIN sigesrac.i001t_localidad loc_hijo ON loc_hijo.co_localidad_padre=loc_padre.co_localidad "
                . " INNER JOIN sigesrac.i002t_tipo_localidad tloc_padre ON tloc_padre.co_tipo_localidad = loc_padre.co_tipo_localidad "
                . " INNER JOIN sigesrac.i002t_tipo_localidad tloc_hijo  ON tloc_hijo.co_tipo_localidad = loc_hijo.co_tipo_localidad AND tloc_hijo.co_tipo_localidad_padre = tloc_padre.co_tipo_localidad"
                . " WHERE loc_hijo.co_localidad_padre=? AND lower(tloc_hijo.tx_tipo_localidad) = ? AND loc_hijo.in_activo='1' ";


        return $this->_exec_query($sql, array($co_localidad_padre, strtolower($tx_tipo_localidad)));
    }

    /**
     * 
     * @param type $co_localidad
     * @return type
     */
    public function selectOficinaAtencion($co_localidad) {
        $fields = "co_oficina_atencion, tx_oficina_atencion";
        return $this->_exec_get_where("i003t_oficina_atencion", $fields, array("co_localidad" => $co_localidad));
    }
    /**
     * 
     * @return type
     */
    public function selectDenominacion() {
        $fields = "co_d_comercial, tx_d_comercial, tx_siglas";
        return $this->_exec_get_where("i023t_denominacion_comercial", $fields, array("in_activo" => "1"));
    }

    /**
     * 
     * @return type
     */
    public function selectTiposTramites() {
        $fields = "co_tipo_tramite, tx_tipo_tramite";
        return $this->_exec_get_where("i027t_tipo_tramite", $fields, array("in_activo" => "1"));
    }

    /**
     * 
     * @param type $co_tipo_tramite
     * @return type
     */
    public function selectMotivosTramites($co_tipo_tramite) {
        $fields = "co_motivo_tramite, co_tipo_tramite, tx_motivo_tramite";
        return $this->_exec_get_where("i026t_motivo_tramite", $fields, array("co_tipo_tramite" => $co_tipo_tramite, "in_activo" => "1"));
    }

    /**
     * 
     * @param type $co_motivo_tramite
     * @return type
     */
    public function selectRequisitos($co_motivo_tramite) {
        $fields = "co_requisito_tramite, co_tipo_tramite, tx_requisito_tramite";
        return $this->_exec_get_where("i028t_requisito_tramite", $fields, array("co_motivo_tramite" => $co_motivo_tramite, "in_activo" => "1"));
    }

    /**
     * 
     * @param type $ins_data
     * @return type
     */
    public function insertContratista($ins_data) {

        if ($this->existContratistaRif($ins_data["co_tipo_rif"], $ins_data["nu_rif"]) === true) {
            return 0;
        }
        $answer = $this->db->insert('i020t_contratista', $ins_data);

        $lastid = ($answer === FALSE) ? -1 : $this->db->insert_id();
        return $lastid;
    }

    /**
     * 
     * @param type $co_contratista
     * @param type $upd_data
     */
    public function updateContratista($co_contratista, $upd_data) {
        $answer = $this->db->update('i020t_contratista', $upd_data, array('co_contratista' => $co_contratista));
        return $answer;
    }

    /**
     * 
     * @param type $co_contratista
     * @param type $upd_data_acceso
     */
    public function updateAutoriza($co_contratista, $upd_data_acceso) {
        $answer = $this->db->update('i020t_contratista', $upd_data_acceso, array('co_contratista' => $co_contratista));
        return $answer;
    }

    /**
     *  
     */
    public function selectRepresentante($co_contratista) {
        $fields = "co_representante_legal, tx_representante_legal, tx_cedula_pasaporte ";
        return $this->_exec_get_where("i021t_representante_legal", $fields, array("co_contratista" => $co_contratista));
    }

    /**
     * 
     * @param type $ins_data
     * @return int
     */
    public function insertRepresentante($ins_data) {
        if ($this->existRepresentanteContratista($ins_data["co_contratista"], $ins_data["tx_cedula_pasaporte"]) === true) {
            return 0;
        }

        $answer = $this->db->insert('i021t_representante_legal', $ins_data);

        $lastid = ($answer === FALSE) ? -1 : $this->db->insert_id();
        return $lastid;
    }

    /**
     * 
     * @param type $co_representante
     * @param type $upd_data_rep
     */
    public function updateRepresentante($co_representante, $upd_data_rep) {

        $answer = $this->db->update("i021t_representante_legal", $upd_data_rep, array('co_representante_legal' => $co_representante));
        return $answer;
    }

    /**
     * 
     * @param type $co_representante_legal
     */
    public function deleteRepresentante($co_representante_legal) {
        $answer = $this->db->delete("i021t_representante_legal", array("co_representante_legal" => $co_representante_legal));

        return $answer;
    }

    /**
     * 
     * @param type $co_contratista
     */
    public function selectHistorialCitas($co_contratista) {
        
        $sql = "SELECT cita.co_cita, cita.co_tipo_tramite, cita.co_motivo_tramite, cita.co_oficina_atencion, cita.fe_fecha_cita, "
                . " tramite.tx_tipo_tramite, motivo.tx_motivo_tramite "
                . "FROM i024t_cita As cita "
                . "INNER JOIN i026t_motivo_tramite As motivo ON motivo.co_motivo_tramite = cita.co_motivo_tramite "
                . "INNER JOIN i027t_tipo_tramite As tramite ON tramite.co_tipo_tramite = cita.co_tipo_tramite "
                . "WHERE cita.co_contratista = ? "
                . "ORDER BY cita.fe_fecha_cita DESC ";
    
      return $this->_exec_query($sql, array($co_contratista));         
                
    }

}

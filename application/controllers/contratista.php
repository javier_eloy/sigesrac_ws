<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contratista
 *
 * @author hernandezjnz
 */
require(APPPATH . 'libraries/REST_Controller.php');

class contratista extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('contratista_model');
    }

    private function _sanitize_update($prm) {
        
    }

    /**
     * get id = Id del Contratista
     */
    public function informacion_get() {

        $result = $this->contratista_model->selectContratista($this->get("id"));

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     *  get trif = Tipo de Rif
     *  get rif  = Nro de Rif
     *  get clave = Clave
     */
    public function autoriza_get() {

        $result = $this->contratista_model->selectUsuarioAutoriza($this->get("trif"), $this->get("rif"), $this->get("pin"));

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     * (sin parametros)
     */
    public function regiones_get() {
        $result = $this->contratista_model->selectLocalidadesMaestras();

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     * 
     */
    public function localidad_get() {
        $result = $this->contratista_model->selectLocalidad($this->get("id"), $this->get("tlocalidad"));

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     *  get id = id de la region
     */
    public function oficina_atencion_get() {
        $result = $this->contratista_model->selectOficinaAtencion($this->get("id"));
        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     *  (sin parametros)
     */
    public function tipos_tramites_get() {
        $result = $this->contratista_model->selectTiposTramites();

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     *  get id  = Ccodgio del tipo de tramite
     */
    public function motivos_tramites_get() {
        $result = $this->contratista_model->selectMotivosTramites($this->get("id"));

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     *  get id = codigo del motivo del tramite
     */
    public function requisitos_get() {
        $result = $this->contratista_model->selectRequisitos($this->get("id"));

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }
    
    /**
     *  Obtiene Denominacion Comercial
     */
    public function denominacion_get() {
        $result = $this->contratista_model->selectDenominacion();

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     * Inserta una nueva contratista
     */
    public function informacion_post() {

        $data = array('co_localidad' => $this->post("co_localidad"),
            'co_d_comercial' => $this->post("co_d_comercial"),
            'co_tipo_rif' => $this->post("co_tipo_rif"),
            'nu_rif' => $this->post("nu_rif"),
            'tx_nombre_contratista' => $this->post("tx_nombre_contratista"),
            'tx_direccion_oficina' => $this->post("tx_direccion_oficina"),
            'tx_ecorreo' => $this->post("tx_ecorreo"),
            'tx_ecorreo_alterno' => $this->post("tx_ecorreo_alterno"),
            'tx_telefono' => $this->post("tx_telefono"),
            'tx_celular' => $this->post("tx_celular"),
            'tx_fax' => $this->post("tx_fax"),
            'pw_contrasena' => $this->post("pw_contrasena"),
            'in_reiniciar_pw' => $this->post("in_reiniciar_pw"),
            'fe_reiniciar_pw' => $this->post("fe_reiniciar_pw"),
            'tx_hash_solicitud' => $this->post("tx_hash_solicitud"));

        $answer = $this->contratista_model->insertContratista($data);

        switch ($answer) {
            case -1 :
                $this->response(NULL, self::HTTP_BAD_REQUEST);
                break;
            case 0:
                $this->response(NULL, self::HTTP_CONFLICT);
                break;
            default:
                $this->response(array(array("co_contratista" => $answer)), self::HTTP_OK);
                break;
        }
    }

    /**
     *  Actualiza una contratista
     */
    public function informacion_put() {

        if (!$this->put("co_contratista")) {
            $this->response($this->put("contratista"), self::HTTP_BAD_GATEWAY);
            exit();
        }

        $data = array('co_localidad' => $this->put("co_localidad"),
            'co_d_comercial' => $this->put("co_d_comercial"),
            'tx_direccion_oficina' => $this->put("tx_direccion_oficina"),
            'tx_ecorreo_alterno' => $this->put("tx_ecorreo_alterno"),
            'tx_telefono' => $this->put("tx_telefono"),
            'tx_celular' => $this->put("tx_celular"),
            'tx_fax' => $this->put("tx_fax"));

        $answer = $this->contratista_model->updateContratista($this->put("co_contratista"), $data);

        if ($answer === true) {
            $this->response(NULL, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Actualiza los datos de autorización de la contrtista
     * 
     */
    function autoriza_put() {
        if (!$this->put("co_contratista")) {
            $this->response(NULL, self::HTTP_BAD_GATEWAY);
            exit();
        }

        $data = array('pw_contrasena' => $this->put("pw_contrasena"),
            'in_reiniciar_pw' => $this->put("in_reiniciar_pw"),
            'fe_reiniciar_pw' => $this->put("fe_reiniciar_pw"),
            'tx_hash_solicitud' => $this->put("tx_hash_solicitud"));
        $answer = $this->contratista_model->updateAutoriza($this->put("co_contratista"), $data);

        if ($answer === true) {
            $this->response(NULL, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_BAD_REQUEST);
        }
    }

    /**
     * 
     */
    function representante_get() {

        if (!$this->get("id")) {
            $this->response(NULL, self::HTTP_BAD_GATEWAY);
            exit();
        }

        $result = $this->contratista_model->selectRepresentante($this->get("id"));
        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

    /**
     * 
     */
    function representante_post() {


        $data = array("co_contratista" => $this->post("co_contratista"),
            "tx_representante_legal" => $this->post("tx_representante_legal"),
            "tx_cedula_pasaporte" => $this->$this->post("tx_cedula_pasaporte"));
        $answer = $this->contratista_model->insertRepresentante($data);

        switch ($answer) {
            case -1 :
                $this->response(NULL, self::HTTP_BAD_REQUEST);
                break;
            case 0:
                $this->response(NULL, self::HTTP_CONFLICT);
                break;
            default:
                $this->response($answer, self::HTTP_OK);
                break;
        }
    }

    /**
     * 
     */
    function representante_put() {

        if (!$this->put("co_representante_legal")) {

            $this->response(NULL, self::HTTP_BAD_GATEWAY);
        }

        $data = array("tx_representante_legal" => $this->put("tx_representante_legal"),
            "tx_cedula_pasaporte" => $this->put("tx_cedula_pasaporte"));

        $answer = $this->contratista_model->updateRepresentante($this->put("co_representante_legal"), $data);

        if ($answer === true) {
            $this->response(NULL, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_BAD_REQUEST);
        }
    }

    /**
     * 
     */
    function representante_delete() {

        if (!$this->delete("id")) {
            $this->response(NULL, sel::HTTP_BAD_GATEWAY);
            exit();
        }

        $answer = $this->contratista_model->deleteRepresentante($this->delete("id"));
        if ($answer === true) {
            $this->response(NULL, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_BAD_REQUEST);
        }
    }

    /*     * *
     * 
     */

    function historial_citas_get() {
        $result = $this->contratista_model->selectHistorialCitas($this->get("id"));

        if (!empty($result)) {
            $this->response($result, self::HTTP_OK);
        } else {
            $this->response(NULL, self::HTTP_NOT_FOUND);
        }
    }

}
